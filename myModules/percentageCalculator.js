function percentage(original, shuffled) {
	var j = 0;

	for (var i = 0; i < 52; i++) {
		if (original[i].Suit == shuffled[i].Suit) {
			j++;
		}

		if (original[i].Rank == shuffled[i].Rank) {
			j++;
		}
	}

	 return j/1.04;

}

module.exports = {
	getPercentage : percentage
};