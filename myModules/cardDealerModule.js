var originalDeck = {

	Suit: ["spades", "diamonds", "hearts", "clubs"],
	Rank: ["ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "jack", "queen", "king"]

};

function sortedDeck() {
	var array = [];

	originalDeck.Suit.forEach(function (suit) {
		originalDeck.Rank.forEach(function (rank) {
			var imgPath = rank + "_of_" +  suit + ".png";
			array.push({
				Suit : suit,
				Rank : rank,
				Img : imgPath
			});
		});
	});

	return array;

}

function shuffleDeck() {

	var array = sortedDeck();
	var index = array.length;
	var temp;
	var randIndex;

	while (index !== 0) {
		randIndex = Math.floor(Math.random() * index);
		index--;

		temp = array[index];
		array[index] = array[randIndex];
		array[randIndex] = temp;
	}

	return array;

}

module.exports = {

	getSortedDeck : sortedDeck,
	getShuffledDeck : shuffleDeck

};

