'use strict'
const express = require('express');
const app = express();
const dealer = require('./myModules/cardDealerModule');
const percentage = require('./myModules/percentageCalculator');
const sortedDeck = dealer.getSortedDeck();
const _ = require('underscore');
const MongoClient = require('mongodb').MongoClient;
let avg;

app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

app.get('/', (req, res) => {
	res.send('Card-dealer API');
});

//defining  overall average percentage from the last element in the DB
MongoClient.connect('mongodb://ulukma:Mynameisuluk21@ds111138.mlab.com:11138/card-dealer-db', (err, client) => {
	console.log("AVG connection");
	let db = client.db('card-dealer-db');

	db.collection('card-dealer-collection')
		.find()
		.limit(1)
		.sort({
			sequence_number: -1
		})
		.project({
			sequence_number: true
		})
		.toArray((err, data) => {
			if (err) {
				console.log('err')
			}
			avg = data[0].avg;

		});
	client.close();
});

MongoClient.connect('mongodb://ulukma:Mynameisuluk21@ds111138.mlab.com:11138/card-dealer-db', (err, client) => {

	console.log("Connected to DB");

	let db = client.db('card-dealer-db');
	//main page route with reading and sending the last element from DB to front end
	app.get('/card', (req, res) => {
		db.collection('card-dealer-collection')
			.find()
			.limit(1)
			.sort({
				sequence_number: -1
			})
			.toArray((err, data) => {
				if (err) {
					console.log('Error on main page');
				}
				res.jsonp(data);
			});
	});

	//page with all elements in DB listed
	app.get('/history', (req, res) => {
		db.collection('card-dealer-collection').find().toArray((err, data) => {
			if (err) {
				console.log('err');
				return;
			}
			res.jsonp(data);
		});
	});

	//returning the last (recently added) element to be dinamicaly pushed on the page
	app.get('/data', (req, res) => {

		db.collection('card-dealer-collection')
			.find()
			.limit(1)
			.sort({
				sequence_number: -1
			})
			.toArray((err, data) => {
				if (err) {
					console.log('Error /data');
				}
				return res.send({
					data: data[0]
				});

			});

	});

	//adding new element to DB
	app.post('/card', (req, res) => {
		let shuffled = dealer.getShuffledDeck();
		let percent = percentage.getPercentage(sortedDeck, shuffled);

		db.collection('card-dealer-collection')
			.find()
			.limit(1)
			.sort({
				sequence_number: -1
			})
			.toArray((err, data) => {
				if (err) {
					console.log('err');
				}

				//defining necessary data for new element
				let newSequenceNumber = ++data[0].sequence_number;
				let newAvg = (data[0].avg * (newSequenceNumber - 1) + percent) / newSequenceNumber;
				db.collection('card-dealer-collection').insertOne({
					sequence_number: newSequenceNumber,
					percentage: percent,
					avg: newAvg,
					configuration: shuffled
				}, function (err) {
					console.log("Successfuly Added");
					res.end();
				});


			});



	});

	//reseting DB with adding sorted deck as the first element of the DB
	app.post('/drop', (req, res) => {
		console.log("Connected for Reset");
		db.collection('card-dealer-collection')
			.drop();

		db.collection('card-dealer-collection')
			.insertOne({
				sequence_number: 1,
				percentage: 100,
				avg: 100,
				configuration: sortedDeck
			}, function (err) {
				console.log('First Element Successfuly Restored')
				res.end();
			});
	});

});


app.listen(process.env.PORT || 5000, () => {
	console.log('Node app is running on ' + process.env.PORT + " or on 5000");
});